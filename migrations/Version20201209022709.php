<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201209022709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE email_mkt_contato_email_mkt_listas (email_mkt_contato_id INT NOT NULL, email_mkt_listas_id INT NOT NULL, INDEX IDX_6103010F46A0BAD8 (email_mkt_contato_id), INDEX IDX_6103010F97A0DA54 (email_mkt_listas_id), PRIMARY KEY(email_mkt_contato_id, email_mkt_listas_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE email_mkt_contato_email_mkt_listas ADD CONSTRAINT FK_6103010F46A0BAD8 FOREIGN KEY (email_mkt_contato_id) REFERENCES email_mkt_contato (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE email_mkt_contato_email_mkt_listas ADD CONSTRAINT FK_6103010F97A0DA54 FOREIGN KEY (email_mkt_listas_id) REFERENCES email_mkt_listas (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE email_mkt_contato_email_mkt_listas');
    }
}
