<?php

namespace App\Controller\EmailMkt;

use App\Entity\EmailMktListas;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ListasEmailMarketingController extends AbstractController
{
    /**
     * @Route("/listas/email/marketing", name="listas_email_marketing")
     */
    public function index()
    {
        return $this->render('erp/emailmkt/listas_email_marketing/listas_email_mkt.html.twig', [
            'controller_name' => 'ListasEmailMarketingController',
        ]);
    }

    /**
     * @Route("/importar/lista/email/marketing", name="importar_lista_email_marketing")
     */
    public function importLits(Request $request): Response
    {

        $form = $this->createFormBuilder()
            ->add('nome', TextType::class, [
                'block_name' => 'nome',
                'label' => false,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3])],
            ])
            ->add('arquivo', FileType::class, [
                'block_name' => 'arquivo',
                'label' => false,
                'attr' => ['style'=>'opacity:0'],

            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $entitymanager = $this->getDoctrine()->getManager();
            $lista = new EmailMktListas();
            $lista->setNome($data['nome']);
            $lista->setCriacao(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $entitymanager->persist($lista);
            $entitymanager->flush();
            $this->addFlash(
                'notice',
                'Seu arquivo esta sendo processado e em breve sua nova lista estará disponível.'
            );
        }

        return $this->render('erp/emailmkt/listas_email_marketing/importar_email_mkt.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
