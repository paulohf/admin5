<?php

namespace App\Entity;

use App\Repository\EmailMktListasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmailMktListasRepository::class)
 */
class EmailMktListas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\Column(type="date")
     */
    private $criacao;

    /**
     * @ORM\ManyToMany(targetEntity=EmailMktContato::class, mappedBy="lista")
     */
    private $contato;

    public function __construct()
    {
        $this->contato = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getCriacao(): ?\DateTimeInterface
    {
        return $this->criacao;
    }

    public function setCriacao(\DateTimeInterface $criacao): self
    {
        $this->criacao = $criacao;

        return $this;
    }

    /**
     * @return Collection|EmailMktContato[]
     */
    public function getContato(): Collection
    {
        return $this->contato;
    }

    public function addContato(EmailMktContato $contato): self
    {
        if (!$this->contato->contains($contato)) {
            $this->contato[] = $contato;
            $contato->addListum($this);
        }

        return $this;
    }

    public function removeContato(EmailMktContato $contato): self
    {
        if ($this->contato->removeElement($contato)) {
            $contato->removeListum($this);
        }

        return $this;
    }
}
