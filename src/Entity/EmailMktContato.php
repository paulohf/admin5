<?php

namespace App\Entity;

use App\Repository\EmailMktContatoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmailMktContatoRepository::class)
 */
class EmailMktContato
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $aniversario;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $telefone;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inscrito;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $motivoSaida;

    /**
     * @ORM\ManyToMany(targetEntity=EmailMktListas::class, inversedBy="contato")
     */
    private $lista;

    public function __construct()
    {
        $this->lista = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAniversario(): ?\DateTimeInterface
    {
        return $this->aniversario;
    }

    public function setAniversario(?\DateTimeInterface $aniversario): self
    {
        $this->aniversario = $aniversario;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function getInscrito(): ?bool
    {
        return $this->inscrito;
    }

    public function setInscrito(bool $inscrito): self
    {
        $this->inscrito = $inscrito;

        return $this;
    }

    public function getMotivoSaida(): ?string
    {
        return $this->motivoSaida;
    }

    public function setMotivoSaida(string $motivoSaida): self
    {
        $this->motivoSaida = $motivoSaida;

        return $this;
    }

    /**
     * @return Collection|EmailMktListas[]
     */
    public function getLista(): Collection
    {
        return $this->lista;
    }

    public function addListum(EmailMktListas $listum): self
    {
        if (!$this->lista->contains($listum)) {
            $this->lista[] = $listum;
        }

        return $this;
    }

    public function removeListum(EmailMktListas $listum): self
    {
        $this->lista->removeElement($listum);

        return $this;
    }
}
