<?php

namespace App\Message;

class EmailMktImportList
{
    private $arquivo;
    private $lista;

    public function __construct(string $arquivo, integer $lista)
    {
        $this->arquivo = $arquivo;
        $this->lista = $lista;
    }

    public function getArquivo(): string
    {
        return $this->arquivo;
    }

    public function getLista(): integer
    {
        return $this->lista;
    }
}