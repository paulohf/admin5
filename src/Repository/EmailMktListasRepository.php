<?php

namespace App\Repository;

use App\Entity\EmailMktListas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailMktListas|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailMktListas|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailMktListas[]    findAll()
 * @method EmailMktListas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailMktListasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailMktListas::class);
    }

    // /**
    //  * @return EmailMktListas[] Returns an array of EmailMktListas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailMktListas
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
