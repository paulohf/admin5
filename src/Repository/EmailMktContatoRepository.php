<?php

namespace App\Repository;

use App\Entity\EmailMktContato;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailMktContato|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailMktContato|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailMktContato[]    findAll()
 * @method EmailMktContato[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailMktContatoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailMktContato::class);
    }

    // /**
    //  * @return EmailMktContato[] Returns an array of EmailMktContato objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailMktContato
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
